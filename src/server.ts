import {createServer, Server} from 'http';
import * as express from 'express';
import * as socketIo from 'socket.io';

import {Message} from './model';

export class ChatServer {
    public static readonly PORT:number = 8080;
    private app: express.Application;
    private server: Server;
    private io: SocketIO.Server;
    private port: string|number;

    constructor() {
        this.app = express();
        this.port = process.env.PORT || ChatServer.PORT;
        this.server = createServer(this.app);
        this.io = socketIo(this.server);
        this.listen();
    }

    private listen(): void {
        this.server.listen(this.port, () => console.log(`Running the SocketIO server on port ${this.port}.`));
        this.io.on('connect', (socket: any) => {
            console.log(`Connected client on port ${this.port}.`);
            socket.on('message', (m: Message) => {
                console.log(`[server] (message): ${JSON.stringify(m)}.`);
                this.io.emit('message', m);
            });
            socket.on('disconnect', () => console.log('Client disconnected.'));
        });
    }

    public getApp(): express.Application {
        return this.app;
    }
}
